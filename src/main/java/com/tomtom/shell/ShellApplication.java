package com.tomtom.shell;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tomtom.shell.command.ShellCommand;
import com.tomtom.shell.command.registry.CommandRegistry;
import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.handler.CommandFaultHandler;
import com.tomtom.shell.printer.OutputPrinter;
import com.tomtom.shell.printer.PrintParams;

@SpringBootApplication
public class ShellApplication implements CommandLineRunner {

	private final Scanner in = new Scanner(System.in);

	@Inject
	private CommandRegistry registry;

	@Inject
	@Named("console")
	private OutputPrinter printer;

	@Inject
	private CommandFaultHandler commandFaultHandler;

	public static void main(String[] args) {
		SpringApplication.run(ShellApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Welcome to tomtom console!!");
		while (true) {
			String command = in.nextLine();
			executeCommand(command);
			// split it by pipe
			// for each command
		}
	}
	

	private void executeCommand(String oneCommand) {
		String[] commandAndparams = oneCommand.split(" ");
		Optional<ShellCommand> commandOptional = registry.getCommand(commandAndparams[0].trim());
		if (commandOptional.isPresent()) {
			ShellCommand shellCommand = commandOptional.get();
			String params = "";
			if (commandAndparams.length > 1) {
				String[] paramsArr = Arrays.copyOfRange(commandAndparams, 1, commandAndparams.length);
				params = String.join(" ", paramsArr);
			}
			try {
				String executionResult = shellCommand.execute(params);
				printer.print(new PrintParams(oneCommand, executionResult));
			} catch (CommandFault e) {
				commandFaultHandler.handleFault(e);
			}
		} else {
			printer.print(new PrintParams(oneCommand, "Invalid Command"));
		}

	}

}
