package com.tomtom.shell.fault;

public abstract class CommandFault extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6307859289716226876L;
	
	public abstract Severity getSeverity();
	
	public abstract String getMessage();
	

}
