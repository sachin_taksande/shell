package com.tomtom.shell.fault;

public class ErrorFault extends CommandFault {

	private static final long serialVersionUID = -5807142456283414234L;
	
	private final String message; 
	
	public ErrorFault(String msg) {
		this.message = msg;
	}

	@Override
	public Severity getSeverity() {
		return Severity.ERROR;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
