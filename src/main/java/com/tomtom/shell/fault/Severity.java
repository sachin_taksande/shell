package com.tomtom.shell.fault;

public enum Severity {

	INFO,
	FATAL,
	ERROR
}
