package com.tomtom.shell.fault;

public class InfoFault extends CommandFault {

	private static final long serialVersionUID = -5807142456283414234L;
	
	private final String message; 
	
	public InfoFault(String msg) {
		this.message = msg;
	}

	@Override
	public Severity getSeverity() {
		return Severity.INFO;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
