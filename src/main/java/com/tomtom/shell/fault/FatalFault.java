package com.tomtom.shell.fault;

public class FatalFault extends CommandFault {

	private static final long serialVersionUID = -5807142456283414234L;
	
	private final String message; 
	
	public FatalFault(String msg) {
		this.message = msg;
	}

	@Override
	public Severity getSeverity() {
		return Severity.FATAL;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
