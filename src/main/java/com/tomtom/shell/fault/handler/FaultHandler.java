package com.tomtom.shell.fault.handler;

import com.tomtom.shell.fault.CommandFault;

public interface FaultHandler {

	boolean accept(CommandFault fault);

	void handleFault(CommandFault fault);

}
