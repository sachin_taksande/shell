package com.tomtom.shell.fault.handler;

import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;

@Component
public class CommandFaultHandler {

	private final Set<FaultHandler> allFaultHandlers;

	@Inject
	public CommandFaultHandler(Set<FaultHandler> allFaultHandlers) {
		this.allFaultHandlers = allFaultHandlers;
	}

	public void handleFault(CommandFault fault) {
		for (FaultHandler handler : allFaultHandlers) {
			if (handler.accept(fault)) {
				handler.handleFault(fault);
			}
		}
	}

}
