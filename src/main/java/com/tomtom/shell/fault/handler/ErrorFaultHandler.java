package com.tomtom.shell.fault.handler;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.Severity;
import com.tomtom.shell.printer.OutputPrinter;
import com.tomtom.shell.printer.PrintParams;

@Component
public class ErrorFaultHandler implements FaultHandler {

	@Inject
	@Named("console")
	private OutputPrinter printer;

	@Override
	public boolean accept(CommandFault fault) {
		return fault.getSeverity() == Severity.ERROR;
	}

	@Override
	public void handleFault(CommandFault fault) {
		printer.print(new PrintParams("ERROR", fault.getMessage()));

	}

}
