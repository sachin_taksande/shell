package com.tomtom.shell.printer;

public interface OutputPrinter {

	void print(PrintParams params);
}
