package com.tomtom.shell.printer;

import org.springframework.stereotype.Component;

@Component("file")
public class FilePrinter implements OutputPrinter {

	@Override
	public void print(PrintParams params) {
		System.out.println("Print in file --> " + params.getCommand() + ": " + params.getMessage());
	}

}
