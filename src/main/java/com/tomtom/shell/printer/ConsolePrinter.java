package com.tomtom.shell.printer;

import org.springframework.stereotype.Component;

@Component("console")
public class ConsolePrinter implements OutputPrinter {

	@Override
	public void print(PrintParams params) {
		System.out.println(params.getMessage());
	}

}
