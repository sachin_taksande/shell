package com.tomtom.shell.command;

import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.inject.Provider;

import org.springframework.stereotype.Component;

import com.tomtom.shell.command.registry.CommandRegistry;
import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.ErrorFault;

@Component
public class HelpCommand implements ShellCommand {

	private final Provider<CommandRegistry> registry;

	public HelpCommand(Provider<CommandRegistry> registry) {
		this.registry = registry;
	}

	@Override
	public String execute(String params) throws CommandFault {
		CommandRegistry commandRegistry = registry.get();
		if (params == null || params.trim().length() == 0) {
			return printWholeCommands();
		}
		String commandToHelpWith = params.trim();
		Optional<ShellCommand> command = commandRegistry.getCommand(commandToHelpWith);
		if (command.isPresent()) {
			return command.get().getHelpString();
		}
		throw new ErrorFault("Invalid command: " + commandToHelpWith);
	}

	private String printWholeCommands() {
		StringBuilder builder = new StringBuilder();
		Set<Entry<String, ShellCommand>> entrySet = registry.get().getAllCommands().entrySet();
		for (Entry<String, ShellCommand> cmd : entrySet) {
			builder.append(cmd.getKey() + ": " + cmd.getValue().getHelpString());
			builder.append("\n");
		}
		return builder.toString();
	}

	@Override
	public String getHelpString() {
		return "command is responsible for printing help content";
	}

	@Override
	public String getName() {
		return "help";
	}

}
