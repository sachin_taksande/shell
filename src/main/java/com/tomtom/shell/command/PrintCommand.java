package com.tomtom.shell.command;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;

@Component
public class PrintCommand implements ShellCommand {

	@Override
	public String execute(String params) throws CommandFault {
		return params;
	}

	@Override
	public String getHelpString() {
		return "command responsible for printing the parameter passed in. e.g. In standard input type 'print Hello World' this will print Hello World on standard output console";
	}

	@Override
	public String getName() {
		return "print";
	}

}
