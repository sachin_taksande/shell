package com.tomtom.shell.command;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.FatalFault;

@Component
public class ExitCommand implements ShellCommand {

	@Override
	public String execute(String params) throws CommandFault {
		throw new FatalFault("Gracefully shutting down");
	}

	@Override
	public String getHelpString() {
		return "command is responsible for exiting the shell";
	}

	@Override
	public String getName() {
		return "exit";
	}

}
