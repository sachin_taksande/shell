package com.tomtom.shell.command;

import com.tomtom.shell.fault.CommandFault;

public interface ShellCommand {
	
	String getName();

	String execute(String params) throws CommandFault;

	String getHelpString();
}
