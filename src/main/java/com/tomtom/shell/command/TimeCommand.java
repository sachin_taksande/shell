package com.tomtom.shell.command;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;

@Component
public class TimeCommand implements ShellCommand {

	@Override
	public String execute(String params) throws CommandFault {
		LocalTime time = LocalTime.now();
		return time.format(DateTimeFormatter.ISO_LOCAL_TIME);
	}

	@Override
	public String getHelpString() {
		return "command is responsible for printing time. e.g. In standard input type 'time' this will print current time like 09:23:00 AM on standard output console";
	}

	@Override
	public String getName() {
		return "time";
	}

}
