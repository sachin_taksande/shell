package com.tomtom.shell.command;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import com.tomtom.shell.fault.CommandFault;

@Component
public class DateCommand implements ShellCommand {

	@Override
	public String execute(String params) throws CommandFault {
		LocalDate time = LocalDate.now();
		return time.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

	@Override
	public String getHelpString() {
		return "command is responsible for printing date";
	}

	@Override
	public String getName() {
		return "date";
	}

}
