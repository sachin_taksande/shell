package com.tomtom.shell.command.registry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.tomtom.shell.command.ShellCommand;

@Component
public class SpringBasedCommandRegistry implements CommandRegistry {

	private final Map<String, ShellCommand> shellCommandMap = new HashMap<>();

	@Inject
	public SpringBasedCommandRegistry(Set<ShellCommand> allCommands) throws Exception {
		for (ShellCommand cmd : allCommands) {
			if (shellCommandMap.containsKey(cmd.getName())) {
				throw new Exception("There exist more than one command handlers for command " + cmd.getName());
			}
			shellCommandMap.put(cmd.getName(), cmd);
		}
	}

	@Override
	public Optional<ShellCommand> getCommand(String commandName) {
		return Optional.ofNullable(shellCommandMap.get(commandName));
	}

	@Override
	public Map<String, ShellCommand> getAllCommands() {
		return Collections.unmodifiableMap(shellCommandMap);
	}

}
