package com.tomtom.shell.command.registry;

import java.util.Map;
import java.util.Optional;

import com.tomtom.shell.command.ShellCommand;

public interface CommandRegistry {

	Optional<ShellCommand> getCommand(String commandName);
	
	Map<String, ShellCommand> getAllCommands();
}
