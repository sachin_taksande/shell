package com.tomtom.shell.fault;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tomtom.shell.command.PrintCommand;

class ErrorFaultTest {

	private ErrorFault testee = new ErrorFault("some error");

	@Test
	void getSeverity_returns_Error() {
		Severity severity = testee.getSeverity();
		assertEquals(Severity.ERROR, severity);
	}

	@Test
	void getMessage_returns_message() {
		String message = testee.getMessage();
		assertEquals("some error", message);
	}

}
