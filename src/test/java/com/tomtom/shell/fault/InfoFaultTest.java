package com.tomtom.shell.fault;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InfoFaultTest {

	private InfoFault testee = new InfoFault("some info");

	@Test
	void getSeverity_returns_Info() {
		Severity severity = testee.getSeverity();
		assertEquals(Severity.INFO, severity);
	}

	@Test
	void getMessage_returns_message() {
		String message = testee.getMessage();
		assertEquals("some info", message);
	}

}
