/**
 * 
 */
package com.tomtom.shell.fault.handler;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.fault.ErrorFault;
import com.tomtom.shell.fault.FatalFault;
import com.tomtom.shell.fault.InfoFault;
import com.tomtom.shell.printer.OutputPrinter;
import com.tomtom.shell.printer.PrintParams;

/**
 * @author Sachin
 *
 */
class FatalFaultHandlerTest {

	@Mock(name = "console")
	OutputPrinter outPrinter;

	@InjectMocks
	FatalFaultHandler testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void accept_whenFatalFault_returns_true() {
		boolean accept = testee.accept(new FatalFault("some fatal error"));
		assertTrue(accept);
	}

	@Test
	void accept_whenInfoFault_returns_false() {
		boolean accept = testee.accept(new InfoFault("some info"));
		assertFalse(accept);
	}

}
