/**
 * 
 */
package com.tomtom.shell.fault.handler;

import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.ErrorFault;

/**
 * @author Sachin
 *
 */
class CommandFaultHandlerTest {

	@Mock
	FaultHandler handler1;

	@Mock
	FaultHandler handler2;

	private CommandFaultHandler testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void handleFault_whenManyFaultHandlers_visitsOnlyFaultHandlersWhichAccept() {
		Mockito.when(handler1.accept(Mockito.any(CommandFault.class))).thenReturn(false);
		Mockito.when(handler2.accept(Mockito.any(CommandFault.class))).thenReturn(true);

		HashSet<FaultHandler> faultHandlers = new HashSet<>();
		faultHandlers.add(handler1);
		faultHandlers.add(handler2);

		testee = new CommandFaultHandler(faultHandlers);
		testee.handleFault(new ErrorFault("some error"));

		Mockito.verify(handler1, Mockito.times(0)).handleFault(Mockito.any(CommandFault.class));
		Mockito.verify(handler2, Mockito.times(1)).handleFault(Mockito.any(CommandFault.class));

	}

}
