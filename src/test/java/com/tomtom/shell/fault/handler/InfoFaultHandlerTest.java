package com.tomtom.shell.fault.handler;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.fault.ErrorFault;
import com.tomtom.shell.fault.InfoFault;
import com.tomtom.shell.printer.OutputPrinter;
import com.tomtom.shell.printer.PrintParams;

class InfoFaultHandlerTest {

	@Mock(name = "console")
	OutputPrinter outPrinter;

	@InjectMocks
	InfoFaultHandler testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void accept_whenInfoFault_returns_false() {
		boolean accept = testee.accept(new ErrorFault("some error"));
		assertFalse(accept);
	}

	@Test
	void accept_whenInfoFault_returns_true() {
		boolean accept = testee.accept(new InfoFault("some info"));
		assertTrue(accept);
	}

	@Test
	void handleFault_printsExpectedMessage() {
		testee.handleFault(new InfoFault("some info"));
		Mockito.verify(outPrinter).print(new PrintParams("INFO", "some info"));
	}


}
