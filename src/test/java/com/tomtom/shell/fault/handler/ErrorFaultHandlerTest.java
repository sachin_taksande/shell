package com.tomtom.shell.fault.handler;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.fault.ErrorFault;
import com.tomtom.shell.fault.InfoFault;
import com.tomtom.shell.fault.Severity;
import com.tomtom.shell.printer.OutputPrinter;
import com.tomtom.shell.printer.PrintParams;

class ErrorFaultHandlerTest {

	@Mock(name = "console")
	OutputPrinter outPrinter;

	@InjectMocks
	ErrorFaultHandler testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void accept_whenErrorFault_returns_true() {
		boolean accept = testee.accept(new ErrorFault("some error"));
		assertTrue(accept);
	}

	@Test
	void accept_whenInfoFault_returns_false() {
		boolean accept = testee.accept(new InfoFault("some info"));
		assertFalse(accept);
	}

	@Test
	void handleFault_printsExpectedMessage() {
		testee.handleFault(new ErrorFault("some error"));
		Mockito.verify(outPrinter).print(new PrintParams("ERROR", "some error"));
	}

}
