package com.tomtom.shell.fault;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FatalFaultTest {

	private FatalFault testee = new FatalFault("some fatal error");

	@Test
	void getSeverity_returns_Fatal() {
		Severity severity = testee.getSeverity();
		assertEquals(Severity.FATAL, severity);
	}

	@Test
	void getMessage_returns_message() {
		String message = testee.getMessage();
		assertEquals("some fatal error", message);
	}

}
