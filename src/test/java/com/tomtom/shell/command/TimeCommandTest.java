package com.tomtom.shell.command;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

import com.tomtom.shell.fault.CommandFault;

class TimeCommandTest {

	private TimeCommand testee = new TimeCommand();

	@Test
	void getName_returns_Time() {
		String name = testee.getName();
		assertEquals("time", name);
	}

	@Test
	void getHelpString_returns_message() {
		String help = testee.getHelpString();
		assertEquals(
				"command is responsible for printing time. e.g. In standard input type 'time' this will print current time like 09:23:00 AM on standard output console",
				help);
	}

	@Test
	void execute_returns_validTime() throws CommandFault {
		String result = testee.execute("anyParam");
		LocalTime localTime = LocalTime.parse(result, DateTimeFormatter.ISO_LOCAL_TIME);
		assertTrue(localTime != null);
	}

}
