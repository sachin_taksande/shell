package com.tomtom.shell.command;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.FatalFault;

class PrintCommandTest {

	private PrintCommand testee = new PrintCommand();

	@Test
	void getName_returns_Print() {
		String name = testee.getName();
		assertEquals("print", name);
	}

	@Test
	void getHelpString_returns_message() {
		String help = testee.getHelpString();
		assertEquals(
				"command responsible for printing the parameter passed in. e.g. In standard input type 'print Hello World' this will print Hello World on standard output console",
				help);
	}

	@Test
	void execute_returns_params() throws CommandFault {
		String result = testee.execute("anyParam");
		assertEquals("anyParam", result);
	}

}
