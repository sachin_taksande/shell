package com.tomtom.shell.command.registry;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.command.ShellCommand;
import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.ErrorFault;
import com.tomtom.shell.fault.FatalFault;
import com.tomtom.shell.fault.handler.CommandFaultHandler;
import com.tomtom.shell.fault.handler.FaultHandler;

class SpringBasedCommandRegistryTest {

	@Mock
	ShellCommand command1;

	@Mock
	ShellCommand command2;

	private SpringBasedCommandRegistry testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void getCommand_whenCommandExists_returnsCommand() throws Exception {
		Mockito.when(command1.getName()).thenReturn("command1");
		Mockito.when(command2.getName()).thenReturn("command2");

		HashSet<ShellCommand> commandSet = new HashSet<>();
		commandSet.add(command1);
		commandSet.add(command2);

		testee = new SpringBasedCommandRegistry(commandSet);
		Optional<ShellCommand> command = testee.getCommand("command2");
		assertTrue(command.isPresent());
		assertSame(command2, command.get());

	}

	@Test
	void getCommand_whenCommandDoesNotExists_returnsNothing() throws Exception {
		Mockito.when(command1.getName()).thenReturn("command1");
		Mockito.when(command2.getName()).thenReturn("command2");

		HashSet<ShellCommand> commandSet = new HashSet<>();
		commandSet.add(command1);
		commandSet.add(command2);

		testee = new SpringBasedCommandRegistry(commandSet);
		Optional<ShellCommand> command = testee.getCommand("command3");
		assertFalse(command.isPresent());
	}

	@Test
	void getAllCommands_returnsUnmodifiableMap() throws Exception {
		Mockito.when(command1.getName()).thenReturn("command1");
		Mockito.when(command2.getName()).thenReturn("command2");

		HashSet<ShellCommand> commandSet = new HashSet<>();
		commandSet.add(command1);
		commandSet.add(command2);

		testee = new SpringBasedCommandRegistry(commandSet);
		Map<String, ShellCommand> allCommands = testee.getAllCommands();
		assertThrows(UnsupportedOperationException.class, () -> {
			allCommands.remove("command1");
		});
	}
}
