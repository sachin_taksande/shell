package com.tomtom.shell.command;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tomtom.shell.fault.FatalFault;

class ExitCommandTest {

	private ExitCommand testee = new ExitCommand();

	@Test
	void getName_returns_Exit() {
		String name = testee.getName();
		assertEquals("exit", name);
	}

	@Test
	void getHelpString_returns_message() {
		String help = testee.getHelpString();
		assertEquals("command is responsible for exiting the shell", help);
	}
	
	@Test
	void execute_throws_fatalFault() {
		Exception exception = assertThrows(FatalFault.class, () -> {
			testee.execute("anyParam");
	    });
		assertEquals("Gracefully shutting down", exception.getMessage());
	}

}
