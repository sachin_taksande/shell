package com.tomtom.shell.command;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Provider;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tomtom.shell.command.registry.CommandRegistry;
import com.tomtom.shell.fault.CommandFault;
import com.tomtom.shell.fault.ErrorFault;
import com.tomtom.shell.fault.FatalFault;

class HelpCommandTest {

	@Mock
	Provider<CommandRegistry> registryProvider;

	@Mock
	CommandRegistry registry;

	@Mock
	ShellCommand command;

	@Mock
	ShellCommand command2;

	@InjectMocks
	HelpCommand testee;

	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void getName_returns_Help() {
		String name = testee.getName();
		assertEquals("help", name);
	}

	@Test
	void getHelpString_returns_message() {
		String help = testee.getHelpString();
		assertEquals("command is responsible for printing help content", help);
	}

	@Test
	void execute_whenValidCommand_returns_params() throws CommandFault {
		Mockito.when(registryProvider.get()).thenReturn(registry);
		Mockito.when(registry.getCommand("anyCommand")).thenReturn(Optional.of(command));
		Mockito.when(command.getHelpString()).thenReturn("Test Command Help");

		String result = testee.execute("anyCommand");
		assertEquals("Test Command Help", result);
	}

	@Test
	void execute_whenInvalidCommand_throws_ErrorFault() throws CommandFault {
		Mockito.when(registryProvider.get()).thenReturn(registry);
		Mockito.when(registry.getCommand("anyCommand")).thenReturn(Optional.of(command));
		Mockito.when(command.getHelpString()).thenReturn("Test Command Help");

		Exception exception = assertThrows(ErrorFault.class, () -> {
			testee.execute("invalidCommand");
		});
		assertEquals("Invalid command: invalidCommand", exception.getMessage());
	}

	@Test
	void execute_whenNoCommand_returns_allParams() throws CommandFault {
		Mockito.when(registryProvider.get()).thenReturn(registry);
		Mockito.when(registry.getCommand("anyCommand")).thenReturn(Optional.of(command));
		Mockito.when(registry.getCommand("anyCommand2")).thenReturn(Optional.of(command2));
		Map<String, ShellCommand> commandMap = new HashMap<>();
		commandMap.put("anyCommand", command);
		commandMap.put("anyCommand2", command2);
		Mockito.when(registry.getAllCommands()).thenReturn(commandMap);
		Mockito.when(command.getHelpString()).thenReturn("Test Command Help");
		Mockito.when(command2.getHelpString()).thenReturn("Test Command Help 2");

		String result = testee.execute("");
		assertEquals("anyCommand: Test Command Help\nanyCommand2: Test Command Help 2\n", result);
	}

}
